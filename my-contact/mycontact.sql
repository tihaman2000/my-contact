create database mycontact
go
use mycontact
go
CREATE TABLE contact (
 id int NOT NULL IDENTITY ,
 name varchar(50) NOT NULL,
 email varchar(50) NULL,
 phone varchar(20) NULL,
 PRIMARY KEY (id)
)